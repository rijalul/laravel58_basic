<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
	protected $table 	= 'kelas';
    protected $fillable = ['nama_kelas','slug_kelas'];
    public function siswas()
    {
        return $this->hasMany('App\Siswa');
    }
}
