<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\Kelas;

class SiswaController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$siswas = Siswa::all();
		return view('siswa.index', compact('siswas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$kelas = Kelas::all();
		return view('siswa.create', compact('kelas'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$data = $request->validate([
			'nis' 		=> 'required|unique:siswas|max:8',
			'nama' 		=> 'required',
			'kelas_id' 	=> 'required',
		]);
		Siswa::create($data);
		return redirect()->route('siswa.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$siswa = Siswa::find($id);
		$kelas = Kelas::all();
		return view('siswa.edit', compact('siswa','kelas'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$data = $request->validate([
			'nis' 		=> 'required|max:8',
			'nama' 		=> 'required',
			'kelas_id' 	=> 'required',
		]);
		Siswa::find($id)->update($data);
		return redirect()->route('siswa.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		Siswa::find($id)->delete();
		return redirect()->back();
	}
}
