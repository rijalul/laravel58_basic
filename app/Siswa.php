<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
	protected $table 	= 'siswas';
	protected $fillable = ['nis','nama','kelas_id'];

	public function kelas()
    {
        return $this->belongsTo('App\Kelas');
    }
}
