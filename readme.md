# Laravel 5.8 BASIC

belajar laravel basic

### permulaan

buka terminal/cmd arahkan ke folder xampp/htdocs

versi ssh
```
git clone git@bitbucket.org:rijalul/laravel58_basic.git
```
versi https
```
git clone https://rijalul@bitbucket.org/rijalul/laravel58_basic.git
```

### copy/duplicate .env.example to .env

silahkan di copy/duplikat file .env.example jadi .env

### setting database
silahkan diisi dibagian 
```
DB_DATABASE=nama_database
DB_USERNAME=username_database(default root)
DB_PASSWORD=password_database(default kosongan)
```
### install composer di cmd
```
composer install
```
### di cmd/terminal
silahkan jalankan key:generate

```
php artisan key:generate
```
maka akan muncul tulisan

```
Application key set successfully.
```
### membuat auth login/register

```
php artisan make:auth
```
### install node terbaru

```
https://nodejs.org/en/download/
```
setelah selesai update node di windows/linux
jalankan
```
npm install
```
lalu
```
npm run dev
```
atau
```
npm run prod
```

### =================================================
### untuk membuat relasi di laravel, saya menggunakan contoh siswa dan kelas, dimana 1 siswa punya 1 kelas. misal si A ada di kelas 2, atau si B ada di kelas 3 atau di X ada di kelas 1
1 kelas banyak siswa
1 siswa 1 kelas saja

### untuk pluck ternyata hanya bisa digunakan di form collective
```
https://laravelcollective.com/docs/5.8/html
```

### cara membuat seeder
```
php artisan make:seed namaTableYangAkanDiBuatSeeder
```
lebih lengkapnya baca dokumentasi laravel
```
https://laravel.com/docs/5.8/seeding#writing-seeders
```
cara menjalankan seed
```
php artisan db:seed
```