const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.styles([
		'resources/css/datatables/dataTables.bootstrap4.css'
	], 'public/css/datatables.css')
	.scripts([
		'resources/js/datatables/jquery.dataTables.js',
	], 'public/js/jquery.datatables.js')
	.scripts([
		'resources/js/datatables/jquery-3.3.1.js'
	], 'public/js/jquery.js')
	.scripts([
		'resources/js/datatables/dataTables.bootstrap4.js'
	], 'public/js/datatables.bs4.js');