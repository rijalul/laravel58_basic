@extends('layouts.app')

@push('styles')
	<link rel="stylesheet" href="{{ asset('css/datatables.css') }}">
@endpush

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">Contoh CRUD</div>

				<div class="card-body">
					code crud di folder <code>resources/views/crud</code> <br><br><br>
					<a class="btn btn-info" href="{{ route('crud.create') }}">Tambah Data Crud</a> <br><br>
					<table id="crudtable" class="table table-striped table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Kode Barang</th>
								<th>Nama Barang</th>
								<th>Harga</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							@php
							$index = 1;
							@endphp
							@foreach($crud as $c)
							<tr>
								<td>{{ $index++ }}</td>
								<td>{{ $c->kode_barang }}</td>
								<td>{{ $c->nama_barang }}</td>
								<td>{{ $c->harga }}</td>
								<td>
									<form action="{{ route('crud.destroy', $c->id) }}" method="POST">
										@csrf
										@method('DELETE')
										<a class="btn btn-warning" href="{{ route('crud.edit', $c->id) }}">Edit</a> <button type="submit" class="btn btn-danger" onclick="return confirm('Yakin hapus data ini?')">Hapus</button>
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>No</th>
								<th>Kode Barang</th>
								<th>Nama Barang</th>
								<th>Harga</th>
								<th>Aksi</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
	<script src="{{ asset('js/jquery.datatables.js') }}"></script>
	<script src="{{ asset('js/datatables.bs4.js') }}"></script>
	<script>
		$(document).ready(function() {
			$('#crudtable').DataTable({
				 "order": [[ 0, "desc" ]]
			});
		} );
	</script>
@endpush