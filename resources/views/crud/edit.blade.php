@extends('layouts.app')

@push('styles')
	{{--  --}}
@endpush

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">Contoh CRUD</div>

				<div class="card-body">
					code crud di folder <code>resources/views/crud</code> <br><br><br>
					
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div><br />
					@endif

					<form action="{{ route('crud.update', $crud->id) }}" method="POST">
						@csrf
						@method('PATCH')
						<div class="form-group">
							<label for="">Kode Barang</label>
							<input type="text" name="kode_barang" class="form-control" placeholder="Kode Barang" value="{{ $crud->kode_barang }}">
						</div>
						<div class="form-group">
							<label for="">Nama Barang</label>
							<input type="text" name="nama_barang" class="form-control" placeholder="Nama Barang" value="{{ $crud->nama_barang }}">
						</div>
						<div class="form-group">
							<label for="">Harga Barang</label>
							<input type="text" name="harga" class="form-control" placeholder="harga Barang" value="{{ $crud->harga }}">
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
	{{--  --}}
@endpush