@extends('layouts.app')

@push('styles')
	{{--  --}}
@endpush

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">Contoh Pluck dengan data siswa dan kelas</div>

				<div class="card-body">
					code pluck() di folder <code>resources/views/siswa</code> <br><br><br>
					
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div><br />
					@endif

					<form action="{{ route('siswa.store') }}" method="POST">
						@csrf
						<div class="form-group">
							<label for="">NIS Siswa</label>
							<input type="number" name="nis" class="form-control" placeholder="NIS Siswa">
						</div>
						<div class="form-group">
							<label for="">Nama Siswa</label>
							<input type="text" name="nama" class="form-control" placeholder="Nama Siswa">
						</div>
						<div class="form-group">
							<label for="">Kelas</label>
							<select name="kelas_id" class="form-control">
								<option value="">pilih salah satu</option>
								@foreach($kelas as $k)
								<option value="{{ $k->id }}">{{ $k->nama_kelas }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
	{{--  --}}
@endpush