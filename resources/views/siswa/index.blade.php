@extends('layouts.app')

@push('styles')
	<link rel="stylesheet" href="{{ asset('css/datatables.css') }}">
@endpush

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">Contoh Pluck dengan data siswa dan kelas</div>

				<div class="card-body">
					code pluck() di folder <code>resources/views/siswa</code> <br><br><br>
					<a class="btn btn-info" href="{{ route('siswa.create') }}">Tambah Data Crud</a> <br><br>
					<table id="siswatable" class="table table-striped table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>No</th>
								<th>NIS</th>
								<th>Nama Siswa</th>
								<th>Kelas</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							@php
							$index = 1;
							@endphp
							@foreach($siswas as $c)
							<tr>
								<td>{{ $index++ }}</td>
								<td>{{ $c->nis }}</td>
								<td>{{ $c->nama }}</td>
								<td>{{ $c->kelas->nama_kelas }}</td>
								<td>
									<form action="{{ route('siswa.destroy', $c->id) }}" method="POST">
										@csrf
										@method('DELETE')
										<a class="btn btn-warning" href="{{ route('siswa.edit', $c->id) }}">Edit</a> <button type="submit" class="btn btn-danger" onclick="return confirm('Yakin hapus data ini?')">Hapus</button>
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>No</th>
								<th>NIS</th>
								<th>Nama Siswa</th>
								<th>Kelas</th>
								<th>Aksi</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
	<script src="{{ asset('js/jquery.datatables.js') }}"></script>
	<script src="{{ asset('js/datatables.bs4.js') }}"></script>
	<script>
		$(document).ready(function() {
			$('#siswatable').DataTable({
				 "order": [[ 0, "desc" ]]
			});
		} );
	</script>
@endpush