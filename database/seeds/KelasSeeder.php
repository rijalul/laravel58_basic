<?php

use Illuminate\Database\Seeder;

class KelasSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('kelas')->insert([
			[
				'nama_kelas' => 'Kelas 1',
				'slug_kelas' => 'kelas-1'
			],
			[
				'nama_kelas' => 'Kelas 2',
				'slug_kelas' => 'kelas-2'
			],
			[
				'nama_kelas' => 'Kelas 3',
				'slug_kelas' => 'kelas-3'
			]
		]);
	}
}
